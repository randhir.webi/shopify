import Shopify, { DataType } from '@shopify/shopify-api';
const axios = require("axios");

const influncerData = async (token,shop) => {
  console.log(token,"-----------------final token----------------------------------------")
    const resData = await axios({
      method: "get",
      url: `https://${shop}/admin/api/2021-07/shop.json`,
      headers: {
        "Content-Type": "application/json",
         "X-Shopify-Access-Token": token,
      },
    });
  
    if (resData) {
     console.log(resData,"---------hhh----------");
     return resData;
    }
  };


  export const saveInfluncerData = async (token, shop) => {
    const shopRes = await influncerData(token, shop);
    if (shopRes) {
       const test = await axios({
        method: "post",
        url:`http://localhost:3000/store/`,
        data: {
          storeName: shopRes.data.shop.name,
          storeUrl: shop,
          storeToken: token,
          storeId: shopRes.data.shop.id,
          storeEmail: shopRes.data.shop.email,
        },
        headers: {
          "Content-Type": "application/json",
        },
      });
    }
  };

 
  
  export const client = async(shop, token) =>{
    const shopifyClient = new Shopify.Clients.Rest(shop, token);
    
    const data = await shopifyClient.post({
      path: 'products',
      data: {"product":{"title":"Burton Custom Freestyle 151","body_html":"\u003cstrong\u003eGood snowboard!\u003c\/strong\u003e","vendor":"Burton","product_type":"Snowboard","images":[{"attachment":"R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==\n"}]}},
      type: DataType.JSON,
    })
    return data
  };


  