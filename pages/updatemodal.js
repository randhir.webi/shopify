import React, { useCallback, useState } from "react";
import { Button, Modal, TextContainer } from "@shopify/polaris";
import Updatelist from "./updatelist";
//import Add from "./addlist";
export default function ModalUpdate(props) {
  const [active, setActive] = useState(true);
  console.log("----prpos---",props);

  const handleChange = useCallback(() => setActive(!active), [active]);

  // const activator = <Button onClick={handleChange}>Open</Button>;

  return (
    <div>
      <Modal
       large={true}
        // activator={activator}
        open={props.isModalUpdate}
        onClose={props.closeModalUpdate}
        //title="Are you sure you want to update list"
        // primaryAction={{
        //   content: "Update",
        //   priimary: true,
        //   onAction: props.closeModalUpdate
        // }}
        // secondaryActions={[
        //   {
        //     content: "cancel",
        //     onAction: props.closeModalUpdate
        //   }
        // ]}
      >
        <Modal.Section>
          <TextContainer>
          <Updatelist updataData={props?.UpdateItem} closemodal={props?.closeModalUpdate}/>
          </TextContainer>
        </Modal.Section>
      </Modal>
    </div>
  );
}
