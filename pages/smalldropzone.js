import React from 'react';
import {DropZone} from '@shopify/polaris';

function  Drop () {
  
    return (
      <div style={{width: 500, height: 100}}>
  <DropZone>
    <DropZone.FileUpload />
  </DropZone>
</div>
    );
}
export default Drop;
