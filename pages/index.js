import React, {useCallback, useState} from 'react';
import {Card, Tabs} from '@shopify/polaris';
import Add from './addlist';
import Update from "./updatelist";
 import  List from "./listpage"
//import { DataTable } from "@shopify/polaris";
import { useRouter } from "next/router";
import Home from "./home"

export default function Tabsinfluencer() {
  const [selected, setSelected] = useState(0);
  const router = useRouter();
  const handleView = () => {
    //event.preventDefault();
    router.push("/addlist");
  };
  const handleTabChange = useCallback(
    (selectedTabIndex) => setSelected(selectedTabIndex),
    [],
  );

  const tabs = [
    {
      id: 'all-customers-1',
      content: 'All  list',
      accessibilityLabel: 'All customers',
      panelID: 'all-customers-content-1',
      component: <List/>,
    
    },
    {
      id: 'accepts-marketing-1',
      content: 'Add  List',
      panelID: 'accepts add list',
      component: <Add/>,
    },
  ];

  return (
    <Card>
      <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}>
        <Card.Section title={tabs[selected].component}>
        </Card.Section>
      </Tabs>
    </Card>
  );
}
