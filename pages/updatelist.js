import React, { useCallback, useEffect, useState } from "react";
//import App from "next/app"
import axios from "axios";
import List from "./listpage";
import { useRouter } from "next/router";
import DropZoneWithImageFileUpload from "./dropzone";
import Toastupdate from "./updateconfirm";
//import {MobileBackArrowMajor} from '@shopify/polaris-icons';
import {
  Button,
  Card,
  FormLayout,
  Layout,
  TextField,
  Pagination,
  Form,
  Toast,
} from "@shopify/polaris";

function updatelist(props) {
  const router = useRouter();
 useEffect(()=>{ console.log("---input---",props,props?.updataData?.id);
 setName(props?.updataData?.name)
 setBio(props?.updataData?.bio)
},[])

  // localhost:3000///

  // add.js // /add
  // home.js // /home
  // index.js // /
  // add:id.js     // add/2
  const handleAdd = (event) => {
    event.preventDefault();
    console.log("getting added");
  };

  const [name, setName] = useState("");
  //const [id, setId] = useState("");
  const [isModalUpdate, setModalUpdate] = useState(false);
  const handleName = useCallback((newName) => setName(newName),  []);
  const [bio, setBio] = useState("");
   const handleBio = useCallback((newBio) => setBio(newBio), []);
  const [status, setStatus] = useState();
/////////////////////////////////

  const handleUpdate = async () => {
    const obj = {
      name: name,
      bio: bio,
      storeName: "store name",
      image: "def",
    };
    console.log(obj, "name--",bio);
    try {
      const postData = await axios.put(
        "http://localhost:3000/influencer/"+props?.updataData?.id,
        obj,
        {
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          },
        }
      );
      setStatus(true);
      props?.closemodal(false)
      console.log("data send", postData);
    } catch (error) {
      console.log("Something is Wrong", error);
    }
  };
  // if (status) {
  // console.log("ttttttttttttttttttttt")
  //     return <Toastupdate/>;

  // }

  return (
    <React.Fragment>
      <div style={{ display: "flex", justifyContent: "right" }}>
        {/* <Button primary onClick={handleBack}>
        Back
      </Button> */}
      </div>
      <Card sectioned>
        <Form onSubmit={handleUpdate}>
          <FormLayout >
            {/* <FormLayout.Group> */}
            <TextField
              label="Name"
              value={name}
              onChange={handleName}
              autoComplete="on"
            />
            <TextField
              label="Bio"
              value={bio}
              multiline={2}
              onChange={handleBio}
              autoComplete="on"
            />
            {/* </FormLayout.Group> */}
            <DropZoneWithImageFileUpload />
            <Button submit primary fullWidth>
              update customer
            </Button>
          </FormLayout>
        </Form>
      </Card>
    </React.Fragment>
  );
}

export default updatelist;
