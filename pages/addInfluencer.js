import React, { useCallback, useState } from "react";
import { useRouter } from "next/router";
import DropZoneWithImageFileUpload from "./dropzone";
import {
  Button,
  Card,
  FormLayout,
  TextField,
  Form,
} from "@shopify/polaris";
const router = useRouter();

function Home (){
    const handleAdd = (event) => {
      event.preventDefault();
     console.log("getting added");
    };
    const handleBack = (e) => {
      e.preventDefault();
      router.push("/");
    };
    const [influencerName, setInfluencerName] = useState("");
    // const handleName = useCallback((newName) => setInfluencerName(newName), []);
    const [bio, setBio] = useState("");
    // const handleBio = useCallback((newBio) => setBio(newBio), []);
    // const handleSubmit = useCallback((_event) => {
    //   setEmail('');
    //   setBio('');
    // }, []);
  
	return (
    <React.Fragment>
      <div style={{ display: "flex", justifyContent: "right" }}>
      <Button primary onClick={handleBack}>
        Back
      </Button>
      </div>
      <Card sectioned>
        <Form  onSubmit={handleSubmit}>
        <FormLayout>
          <TextField
            label="Name"
            type="email"
            placeholder="Enter your name"
            value={influencerName}
            // onChange={handleName}
            autoComplete="on"
          />
          <TextField
            label="Bio"
            placeholder="Enter your Biography"
            value={bio}
            multiline={1}
            // onChange={handleBio}
            autoComplete="on"
          />
          <DropZoneWithImageFileUpload />
          <Button submit primary fullWidth onClick={handleAdd}>Add customer</Button>
        </FormLayout>
        </Form>
      </Card>
    </React.Fragment>
  );
}

export default Home;
