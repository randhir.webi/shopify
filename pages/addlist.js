import React, { useCallback, useEffect, useState } from "react";
//import App from "next/app"
import axios from "axios";
import List from "./listpage";
import { useRouter } from "next/router";
import Toastupdate from "./updateconfirm";
import DropZoneWithImageFileUpload from "./dropzone";
//import {MobileBackArrowMajor} from '@shopify/polaris-icons';
import {
  Button,
  Card,
  FormLayout,
  Layout,
  TextField,
  Pagination,
  Form,
  Toast,
} from "@shopify/polaris";

function add(props) {
  const router = useRouter();

  // localhost:3000///

  // add.js // /add
  // home.js // /home
  // index.js // /
  // add:id.js     // add/2
  const handleAdd = (event) => {
    event.preventDefault();
    console.log("getting added");
  };
  // const handleBack = (e) => {
  //   e.preventDefault();
  //   router.push("/");
  // };
  // const [active, setActive] = useState(false);
  // const toastMarkup = active ? (
  //   <Toast content="Message sent" onDismiss={active} />
  // ) : null;
 const [active, setActive] = useState(false);
  const [name, setName] = useState("");
  const [NameError, setNameError] = useState("");
  const [BioError, setBioError] = useState("");
  // const handleName = useCallback((newName) => setName(newName),  []);
  const [bio, setBio] = useState("");
  // const handleBio = useCallback((newBio) => setBio(newBio), []);
  const [status, setStatus] = useState();

  const handleName = (e) => {
    setNameError("");
    setName(e);
  };

  const handleBio = (e) => {
    setBioError("");
    setBio(e);
  };

  const handleSubmit = async () => {
    const obj = {
      name: name,
      bio: bio,
      storeName: " ",
      image: "def",
    };
    console.log(obj, "name---", name);
    if (name == "") {
      setNameError("fieled is required ");
    }
    if (bio == "") {
      setBioError("fieled is required ");
    }

    try {
      const postData = await axios.post(
        `http://localhost:3000/influencer`,
        obj,
        {
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          },
        }
      );
      <Toastupdate/>
      setStatus(true);
      // {toastMarkup}
      console.log("data send", postData);
    } catch (error) {
      console.log("Something is Wrong", error);
    }
  };
  if (status) {
  
  return <List/>;
  // useEffect(()=>{
  //   router.push("/");
  // })
  }
  // useEffect(() => {
  // <Toastupdate/>
  // }, []);

  return (
    <React.Fragment>
      <div style={{ display: "flex", justifyContent: "right" }}>
        {/* <Button primary onClick={handleBack}>
        Back
      </Button> */}
      </div>
      <Card sectioned>
        <Form onSubmit={handleSubmit}>
          <FormLayout>
            {/* <FormLayout.Group> */}
            <TextField
              requiredIndicator="true"
              label="Name"
              type="text"
              placeholder="Enter your name"
              value={name}
              onChange={handleName}
              autoComplete="on"
              error={NameError}
            />
            <TextField
              requiredIndicator="true"
              label="Bio"
              type="text"
              placeholder="Enter your Biography"
              value={bio}
              multiline={1}
              onChange={handleBio}
              autoComplete="on"
              error={BioError}
              //error=" name is required"
            />
            {/* </FormLayout.Group> */}
            <DropZoneWithImageFileUpload />
            <Button submit primary fullWidth>
              Add customer
            </Button>
          </FormLayout>
        </Form>
      </Card>
    </React.Fragment>
  );
}

export default add;
