import React, { useCallback, useEffect, useState } from "react";
import { EditMajor, DeleteMajor, ViewMinor } from "@shopify/polaris-icons";
import Update from "./updatelist";
import ListDelete from "./listdelete";
import ModalUpdate from "./updatemodal";
import Toastupdate from "./updateconfirm";
import axios from "axios"

//import { DataTable } from "@shopify/polaris";
import { useRouter } from "next/router";
import {
  useIndexResourceState,
  Card,
  IndexTable,
  TextStyle,
  Icon,
  Button,
  DataTable,
  Tooltip,
} from "@shopify/polaris";
//import list from "../data.json";

export default function SimpleIndexTableExample({}) {

  const [isModalShow, setModalView] = useState(false);
  const [isModalUpdate, setModalUpdate] = useState(false);
  const [list, setList] = useState([]);
  const [status, setStatus] = useState("");
  const [selectid, setSelectedid] = useState("");
  const [updatedata, setUpdatedata] = useState(null);
  const router = useRouter();
  const handleClickAdd = (event) => {
    console.log("button onClick working");
    event.preventDefault();
    router.push("/addlist");
  };
  // console.log(updateId)
  const deleteItem = async () => {
    console.log("hello", selectid);
  //   const obj = {  
  //     selectid: selectid
     
  // }
    try {
      const deleteData= await axios.delete(`http://localhost:3000/influencer/`+selectid,{
       headers: {
           'Content-Type': 'application/json',
           'Access-Control-Allow-Origin' : '*'
       }
      
      })
      
      //setStatus(true);
      console.log("data delete",deleteData);
      if(deleteData?.status===200){
      
        console.log("successfully deleted");
        apiCall();
       
      }
      else{
        console.log("uanble to delete record",deleteData);
        setModalView(false);
      }
     
    } catch (error) {
       console.log("Something is Wrong",error);
      }
      setModalView(false);
     
  };

  useEffect(() => {
    apiCall();
  }, []);

  const apiCall = () =>
    fetch("http://localhost:3000/influencer")
      .then((res) => res.json())
      .then((json) => {
        console.log(json, "-------response data----------");
        setList(json);
      });

  // const handleEdit = (title, description, image) => {
  //   // const handleClick = (data) => {
  //   //e.preventDefault();
  //   router.push({
  //     pathname: "/updatelist",
  //     // query: {
  //     //   name: JSON.stringify(title),
  //     //   bio: JSON.stringify(description),
  //     //   //image: JSON.stringify(image),
  //     // },
  //   });
  //   // };
  //   console.log(title, description);
  // };
  const handleEdit = () => {
    // e.preventDefault();
    router.push("/editlist");
    console.log("edit is working");
  };
  const handleView = () => {
    // e.preventDefault();
    router.push("/home");
    console.log("view is working");
  };
  // const handleUpdate = (name) => {
  //   console.log(name);
  // };
  const resourceName = {
    singular: "customer",
    plural: "customers",
  };
  //   ---------------
  //   <div style={{padding: '75px 0'}}>
  //   <Tooltip active content="This order has shipping labels.">
  //     <TextStyle variation="strong">Order #1001</TextStyle>
  //   </Tooltip>
  //
  // </div>
  //   ---------------
  const rows = list.map(({ _id, bio, name,image }, index) => [
    index + 1,
    name,
    [`${bio.substring(0, 110)}.......`],
    <div style={{ display: "flex" }}>
      <div className="view" onClick={() => handleView("", "")}>
        <Icon source={ViewMinor} color="base" />
      </div>
      <div className="view" shopdata={_id,bio,name} onClick={() => {
          setModalUpdate(true),  setUpdatedata({id:_id,name:name,bio:bio});
        }}>
        <Icon source={EditMajor} color="base" />
      </div>
      <div
        className="view"
        onClick={() => {
          setModalView(true), setSelectedid(_id);
        }}
      >
        <Icon source={DeleteMajor} color="base" />
      </div>
    </div>,
  ]);

  // const {
  //   selectedResources,
  //   allResourcesSelected,
  //   handleSelectionChange,
  // } = useIndexResourceState(list);

  // const rowMarkup = list.map(
  //   ({ id,description, image,title }, index) => (
  //     <IndexTable.Row
  //       id={id}
  //       // key={id}
  //       selected={selectedResources.includes(id)}
  //       position={index}
  //      >
  //       <IndexTable.Cell>{title}</IndexTable.Cell>
  //       <IndexTable.Cell>
  //         {/* {" "} */}
  //         <div onClick={handleEdit} id={id}>
  //         <Icon source={EditMajor} color="base" />
  //         </div>
  //       </IndexTable.Cell>
  //       <IndexTable.Cell>
  //         {/* {" "} */}
  //         <Icon source={DeleteMajor} color="base" />
  //       </IndexTable.Cell>
  //       {/* <IndexTable.Cell>{amountSpent}</IndexTable.Cell> */}
  //     </IndexTable.Row>
  //   )
  // );

  return (
    <React.Fragment>
      <div>
        {/* <div style={{ display: "flex", justifyContent: "right" }}>
          <Button primary onClick={(e) => handleClickAdd(e)}>
            Add list
          </Button>
        </div> */}
        <Card>
          <div
            style={{
              width: 1,
              height: 0,
            }}
          >
            <ListDelete
              isModalShow={isModalShow}
              closeModal={() => setModalView(true)}
              deleteItem={deleteItem}
              closeModal={() => setModalView(false)}
            />
          </div>
          <div
            style={{
              width: 1,
              height: 0,
            }}
          >
            <ModalUpdate
               isModalUpdate={isModalUpdate}
               closeModalUpdate={() => setModalUpdate(true)}
               UpdateItem={updatedata}
              closeModalUpdate={() => setModalUpdate(false)}
            />
          </div> 
          <DataTable
        
            columnContentTypes={[
              "text",
              "text",
              "text",
              "text",
              //  'numeric',
              //  'numeric',
            ]}
            headings={[
              "S.no",
              "Name",
              "Bio",
              "Action",
              // 'Delete',
            ]}
            rows={rows}
            // totals={['', '', '', 255, '$155,830.00']}
          />
        </Card>
      </div>
    </React.Fragment>
  );
}
// export async function getStaticProps() {
//console.log("hello");
//   const res = await fetch("https://9bff-125-99-173-186.ngrok.io/influencer/")
//   const data = await res.json()
//   console.log(data)
//   return {
//     props: {
//       data
//     }
//   }
// }
