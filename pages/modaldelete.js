import React, { useCallback, useState } from "react";
import { Button, Modal, TextContainer } from "@shopify/polaris";

export default function ModalDelete(props) {
  const [active, setActive] = useState(true);
  console.log("----prpos---",props);

  const handleChange = useCallback(() => setActive(!active), [active]);

  // const activator = <Button onClick={handleChange}>Open</Button>;

  return (
    <div style={{ height: "500px" }}>
      <Modal
        // activator={activator}
        open={props.isModalShow}
        onClose={props.closeModal}
        title="Are you sure you want to delete this image?"
        primaryAction={{
          content: "Delete",
          destructive: true,
          onAction: props.deleteAction
        }}
        secondaryActions={[
          {
            content: "cancel",
            onAction: props.closeModal
          }
        ]}
      >
        {/* <Modal.Section>
          <TextContainer>
            <p>
              Use Instagram posts to share your products with millions of
              people. Let shoppers buy from your store without leaving
              Instagram.
            </p>
          </TextContainer>
        </Modal.Section> */}
      </Modal>
    </div>
  );
}
