import React, {useCallback, useState} from 'react';
import {Button, Frame, Page, Toast} from '@shopify/polaris';

export default function Toastupdate(props) {
  const [active, setActive] = useState(true);
  //const toggleActive = useCallback(() => setActive((active) => !active), []);

  const toastMarkup = active ? (
    <Toast content="Data Updated"  duration={3000} />
  ) : null;

  return (
    <div style={{height: '250px'}}>
      <Frame>
        {/* <Page> */}
          {toastMarkup}
        {/* </Page> */}
      </Frame>
    </div>
  );
}
